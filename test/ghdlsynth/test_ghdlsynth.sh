#!/bin/sh
opts=--std=08
vhdldir=`realpath ../../rtl/vhdl`
ghdl -a $opts $vhdldir/T80_Pack.vhd
ghdl -a $opts $vhdldir/T80_ALU.vhd
ghdl -a $opts $vhdldir/T80_Reg.vhd
ghdl -a $opts $vhdldir/T80_MCode.vhd
ghdl -a $opts $vhdldir/T80.vhd
ghdl -a $opts $vhdldir/T80se.vhd
ghdl -a $opts $vhdldir/Z80se.vhd
ghdl -r $opts Z80se --no-run
yosys -m ghdl <<EOF
ghdl $opts Z80se
EOF
