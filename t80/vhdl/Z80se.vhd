library IEEE;
use IEEE.std_logic_1164.all;
library work;
use work.T80_Pack.all;

entity Z80se is
	port(
		CLK          : in std_logic;
		RESET_n      : in std_logic;
		CE           : in std_logic := '1';

		A           : out std_logic_vector(15 downto 0);
		DI           : in std_logic_vector(7 downto 0);
		DO           : out std_logic_vector(7 downto 0);

		MREQ_n       : out std_logic;
		IORQ_n       : out std_logic;
		RD_n         : out std_logic;
		WR_n         : out std_logic;

		WAIT_n       : in std_logic := '1';
		BUSRQ_n      : in std_logic := '1';
		BUSAK_n      : out std_logic;
		INT_n        : in std_logic := '1';
		NMI_n        : in std_logic := '1';

                M1_n         : out std_logic;
                REFSH_n      : out std_logic;
                HALT_n       : out std_logic
	);
end Z80se;

architecture SYN of Z80se is
begin

	Z80_uP : T80se
		generic map
		(
			Mode => 0      -- Z80
		)
		port map
		(
			RESET_n     => RESET_n,
			CLK_n       => CLK,
			CLKEN       => CE,
			WAIT_n      => WAIT_n,
			INT_n       => INT_n,
			NMI_n       => NMI_n,
			BUSRQ_n     => BUSRQ_n,
			M1_n        => M1_n,
			MREQ_n      => MREQ_n,
			IORQ_n      => IORQ_n,
			RD_n        => RD_n,
			WR_n        => WR_n,
			RFSH_n      => REFSH_n,
			HALT_n      => HALT_n,
			BUSAK_n     => BUSAK_n,
			A           => A,
			DI          => DI,
			DO          => DO
		);

end architecture SYN;
