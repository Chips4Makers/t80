import os, textwrap
from enum import Enum

from nmigen import *
from nmigen.lib.io import *

from nmigen_soc.wishbone.bus import Interface

__all__ = ["T80Mode", "T80", "T80_WB", "Z80", "Z80_WB"]


class T80Mode(Enum):
    Z80 = 0
    FASTZ80 = 1
    I8080 = 2
    GB = 3


class T80(Elaboratable):
    """nMigen wrapper around T80 VHDL code.

    T80a* modules are not wrapped in nMigen code as they use tri-state logic internally.

    Args:
        mode (T80Mode): mode for operation.
        t2write (bool): default is ``False``.
            If ``True`` read/write attributes are asserted in T3 otherwise T2.
        iowait (bool): default is ``True``.
            If ``True`` use Std I/O cycle, otherwise single cycle I/O
        domain (str): default is ``"sync"``
            The clock domain in which the CPU will reside.
        on_negedge (bool, optional): default is False
            If ``True`` inputs to the block will be latched on the falling edge
            of the clock and output will also change on the edge. This can allow the use
            of synchronous SRAMs as memory without the need for wait states. This is possible
            if one latches the address in the synchronous SRAM on the rising edge of the clock
            and the output of the SRAM available on the next falling edge of the clock.

    As is common in nMigen all logic is active high and active low signal on the CPU have been
    converted where needed.

    Attributes for signals of Z80 CPU:
        wait (Signal(1)): corresponds with WAIT pin
        irq (Signal(1)): corresponds with INT pin
        nmi (Signal(1): corresponds with NMI pin
        bus_req (Signal(1)): corresponds with BUSRQ pin
        bus_ack (Signal(1)): corresponds with BUSAK pin
        m1 (Signal(1)): corresponds with M1 pin
        mem_req (Signal(1)): corresponds with MREQ pin
        io_req (Signal(1)): corresponds with IORQ pin
        read (Signal(1)): corresponds with RD pin
        write (Signal(1)): corresponds with WR pin
        refresh (Signal(1)): corrsponds with RFSH pin
        halt (Signal(1)): correponds with HALT pin
        addr (Signal(16)): corresponds with A0-A15 pins
        data (Record((("i", 8), ("o", 8)))): corresponds with D0-D7 pins.
            The input value from a read has to assigned to the ``i`` field. The output to be written
            during a write cycle is given in the ``o`` field.

    Extra attributes:
        ce (Signal(1)): if not asserted clock to CPU is gated; at reset signal will be asserted.
        en (Signal(1)): if not asserted CPU will be kept in reset state; at reset signal will be
            asserted.
    """
    @staticmethod
    def _add_files(platform, prefix):
        d = os.path.realpath("{dir}{sep}{par}{sep}vhdl".format(
            dir=os.path.dirname(__file__), sep=os.path.sep, par=os.path.pardir
        )) + os.path.sep
        for fname in [
            "T80_Pack.vhd",
            "T80_Reg.vhd",
            "T80_MCode.vhd",
            "T80_ALU.vhd",
            "T80.vhd",
            "T80se.vhd",
            "Z80se.vhd",
        ]:
            f = open(d + fname, "r")
            platform.add_file(prefix + fname, f)
            f.close()


    _cell_templ = textwrap.dedent(r"""
    library ieee;
    use ieee.std_logic_1164.ALL;
    
    use work.T80_Pack.ALL;
    
    entity {name} is
    	port(
    		RESET_n      : in std_logic;
    		CLK_n        : in std_logic;
    		CLKEN        : in std_logic;
    		WAIT_n       : in std_logic;
    		INT_n        : in std_logic;
    		NMI_n        : in std_logic;
    		BUSRQ_n      : in std_logic;
    		M1_n         : out std_logic;
    		MREQ_n       : out std_logic;
    		IORQ_n       : out std_logic;
    		RD_n         : out std_logic;
    		WR_n         : out std_logic;
    		RFSH_n       : out std_logic;
    		HALT_n       : out std_logic;
    		BUSAK_n      : out std_logic;
    		A            : out std_logic_vector(15 downto 0);
    		DI           : in std_logic_vector(7 downto 0);
    		DO           : out std_logic_vector(7 downto 0)
    	);
    end {name};
    
    architecture rtl of {name} is
    begin
    
    	uP : T80se
    		generic map
    		(
    			Mode => {mode},
    			T2Write => {t2write},
    			IOWait => {iowait}
    		)
    		port map
    		(
    			RESET_n     => RESET_n,
    			CLK_n       => CLK_n,
    			CLKEN       => CLKEN,
    			WAIT_n      => WAIT_n,
    			INT_n       => INT_n,
    			NMI_n       => NMI_n,
    			BUSRQ_n     => BUSRQ_n,
    			M1_n        => M1_n,
    			MREQ_n      => MREQ_n,
    			IORQ_n      => IORQ_n,
    			RD_n        => RD_n,
    			WR_n        => WR_n,
    			RFSH_n      => RFSH_n,
    			HALT_n      => HALT_n,
    			BUSAK_n     => BUSAK_n,
    			A           => A,
    			DI          => DI,
    			DO          => DO
    		);
    
    end architecture rtl;
    """)
    @classmethod
    def _add_instance(cls, platform, prefix, *, mode, t2write, iowait):
        name = "t80se_mode{}{}".format(mode.value, "" if not iowait else "_iowait")

        platform.add_file(
            "{}{}.vhdl".format(prefix, name),
            cls._cell_templ.format(
                name=name, mode=mode.value, t2write=int(t2write), iowait=int(iowait)
            )
        )

        return name


    def __init__(self, mode=T80Mode.Z80, *, t2write=False, iowait=True, domain="sync", on_negedge=False):
        self.wait = Signal()
        self.irq = Signal()
        self.nmi = Signal()
        self.bus_req = Signal()
        self.bus_ack = Signal()
        self.m1 = Signal()
        self.mem_req = Signal()
        self.io_req = Signal()
        self.read = Signal()
        self.write = Signal()
        self.refresh = Signal()
        self.halt = Signal()
        self.addr = Signal(16)
        self.data = Record((("i", 8), ("o", 8)))
        self.ce = Signal(reset=1) # If ce is 0, the chip will not be clocked
        self.en = Signal(reset=1) # If en is low, chip will be kept in reset state

        ##

        self._mode = mode
        self._t2write = t2write
        self._iowait = iowait
        self._domain = domain
        self._on_negedge = on_negedge

    def elaborate(self, platform):
        self.__class__._add_files(platform, "t80/")
        cell = self.__class__._add_instance(
            platform, "t80/", mode=self._mode, t2write=self._t2write, iowait=self._iowait
        )

        m = Module()

        clk = Signal(reset_less=True)
        reset_n = Signal(reset_less=True)
        wait_n = Signal(reset_less=True)
        int_n = Signal(reset_less=True)
        nmi_n = Signal(reset_less=True)
        busrq_n = Signal(reset_less=True)
        m1_n = Signal(reset_less=True)
        mreq_n = Signal(reset_less=True)
        iorq_n = Signal(reset_less=True)
        rd_n = Signal(reset_less=True)
        wr_n = Signal(reset_less=True)
        rfsh_n = Signal(reset_less = True)
        halt_n = Signal(reset_less = True)
        busak_n = Signal(reset_less = True)
        m.d.comb += [
            clk.eq(ClockSignal(self._domain) if not self._on_negedge else ~ClockSignal(self._domain)),
            reset_n.eq(~(ResetSignal(self._domain) | ~self.en)),
            wait_n.eq(~self.wait),
            int_n.eq(~self.irq),
            nmi_n.eq(~self.nmi),
            busrq_n.eq(~self.bus_req),
            self.m1.eq(~m1_n),
            self.mem_req.eq(~mreq_n),
            self.io_req.eq(~iorq_n),
            self.read.eq(~rd_n),
            self.write.eq(~wr_n),
            self.refresh.eq(~rfsh_n),
            self.halt.eq(~halt_n),
            self.bus_ack.eq(~busak_n),
        ]

        m.submodules.t80 = Instance(cell,
            i_reset_n = reset_n,
            i_clk_n = clk,
            i_clken = self.ce,
            i_wait_n = wait_n,
            i_int_n = int_n,
            i_nmi_n = nmi_n,
            i_busrq_n = busrq_n,
            o_m1_n = m1_n,
            o_mreq_n = mreq_n,
            o_iorq_n = iorq_n,
            o_rd_n = rd_n,
            o_wr_n = wr_n,
            o_rfsh_n = rfsh_n,
            o_halt_n = halt_n,
            o_busak_n = busak_n,
            o_a = self.addr,
            i_di = self.data.i,
            o_do = self.data.o,
        )

        return m

class T80_WB(Elaboratable):
    """nMigen wrapper around T80 VHDL code with Wishbone interface.

    This is based on :class:`T80` but some of the attributes are now handled by a Wishbone
    interface. :attr:`T80.addr`, :attr:`T80.data`, :attr:`T80.wait`, :attr:`T80.bus_req`,
    :attr:`T80.bus_ack`, :attr:`T80.read` and :attr:`T80.write` are now handled by a
    :attr:`bus` attribute that is a ``nmigen_soc.Wishbone.Interface`` with "stall" and "lock" features.

    For the parameters :arg:`mode`, :arg:`t2write`, :arg:`iowait`, :arg:`domain` and :arg:`on_negedge`
    and the attributes :attr:`m1`, :attr:`io_req`, :attr:`mem_req`, :attr:`halt`, :attr:`irq`,
    :attr:`nmi`, :attr:`ce` and :attr:`en` see documentation of :class:`T80`.
    """
    def __init__(self, mode=T80Mode.Z80, *, t2write=False, iowait=True, domain="sync", on_negedge=True):
        self._t80 = T80(mode=mode, t2write=t2write, iowait=iowait, domain=domain, on_negedge=on_negedge)
        self.bus = Interface(data_width=8, addr_width=16, features=("stall", "lock"))

        self.m1 = Signal()
        self.io_req = Signal()
        self.mem_req = Signal()
        self.halt = Signal()
        self.irq = Signal()
        self.nmi = Signal()
        self.ce = Signal(reset=1)
        self.en = Signal(reset=1)

    def elaborate(self, platform):
        wb = self.bus

        m = Module()

        m.submodules.t80_ = t80 = self._t80
        m.d.comb += [
            t80.irq.eq(self.irq),
            t80.nmi.eq(self.nmi),
            t80.bus_req.eq(0),
            self.m1.eq(t80.m1),
            self.mem_req.eq(t80.mem_req),
            self.io_req.eq(t80.io_req),
            wb.we.eq(t80.write),
            self.halt.eq(t80.halt),
            wb.adr.eq(t80.addr),
            t80.data.i.eq(wb.dat_r),
            wb.dat_w.eq(t80.data.o),
            t80.ce.eq(self.ce),
            t80.en.eq(self.en),
        ]

        # Do a Wishbone cycle when read or write and not refresh
        cyc = (t80.read | t80.write) & ~t80.refresh & ~t80.halt
        # wait when Wishbone is stalled or a Wishbone cycle was done in previous cycle
        # and no ack is received yet.
        cyc_prev = Signal(reset = 0)
        m.d.sync += cyc_prev.eq(cyc & ~wb.stall)
        wait = wb.stall | (cyc_prev & ~wb.ack)
        m.d.comb += [
            t80.wait.eq(wait),
            wb.cyc.eq(cyc | wait | wb.ack),
            wb.stb.eq(cyc),
        ]

        return m


def Z80(*, fast=False, t2write=False, iowait=True, domain="sync", on_negedge=False):
    """Will create a :class:`T80` object of Z80 mode

    Args:
        fast (bool): default is ``False``.
            If ``False`` T80 will of mode ``T80Mode.Z80`` otherwise ``T80Mode.FASTZ80``
        t2write (bool): see :class:`T80`.
        iowait (bool): see :class:`T80`.
        domain (str): see :class:`T80`.
        on_negedge (bool): see :class:`T80`.
    """
    mode = T80Mode.Z80 if not fast else T80Mode.FASTZ80
    return T80(mode, t2write=t2write, iowait=iowait, domain=domain, on_negedge=on_negedge)

def Z80_WB(*, fast=False, t2write=False, iowait=True, domain="sync", on_negedge=True):
    """Will create a :class:`T80_WB` object of Z80 mode

    Args:
        fast (bool): default is ``False``.
            If ``False`` T80 will of mode ``T80Mode.Z80`` otherwise ``T80Mode.FASTZ80``
        t2write (bool): see :class:`T80_WB`.
        iowait (bool): see :class:`T80_WB`.
        domain (str): see :class:`T80_WB`.
        on_negedge (bool): see :class:`T80_WB`.
    """
    mode = T80Mode.Z80 if not fast else T80Mode.FASTZ80
    return T80_WB(mode, t2write=t2write, iowait=iowait, domain=domain, on_negedge=on_negedge)
